from django.shortcuts import render
from django.contrib.auth.models import User
from mysite.models import customer,category,food_item,cart,order,delivery_partner,Restaurant,feedback
from django.http import HttpResponse, HttpResponseRedirect,JsonResponse
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
import datetime
from django.contrib.auth.decorators import login_required
import random
from django.contrib.auth.hashers import check_password
all_categories = category.objects.all()
from django.core.mail import EmailMessage

import random
## recent items
recents = food_item.objects.all().order_by('-id')[:3]
all=[]
all_cats =category.objects.all()[:7]
new_cust = customer.objects.all().order_by('-id')[:6]
new_rests = Restaurant.objects.all().order_by('-id')[:6]
for i in recents:
        item={
                'id':i.id,
                'course':i.course,
                'rest':i.rest_id.name,
                'name':i.item_name,
                'cat':i.category_id.name,
                'ing':i.ingredients.split('@'),
                'price':i.price,
                'status':i.status,
                'preparation':i.preparation_time,
                'image':i.image,
        }
        all.append(item)

def index(request):
    return render(request,'index.html',{'cat':all_categories,'all':all,'ncat':all_cats,'nrest':new_rests})

def signupdata(request):
    if request.method == 'POST':
        email = request.POST['email']
        name = request.POST['Name']
        password = request.POST['password']
        contact = request.POST['contact']
        city = request.POST['city']

        user_obj = User.objects.create_user(email,email,password)
        user_obj.first_name = name
        user_obj.save()

        cust = customer(user=user_obj,contact=contact,city=city)
        cust.save()
        return render(request,'index.html',{'status':'Registred Successfully!!','all':all,'ncat':all_cats,'nrest':new_rests})

def check_user(request):
    un = request.GET['username']
    check = User.objects.filter(username=un)
    if len(check)!=0:
        return HttpResponse("A user with this email already exists!!")
    else:
        return HttpResponse("")

def get_item(request):
    id = request.GET['id']
    if 'rid' in request.GET:
        rid = request.GET['rid']
        cat = category.objects.get(id=id)
        items = food_item.objects.filter(category_id=cat,rest_id=rid).values()
    
        return JsonResponse(list(items),safe=False)
    cat = category.objects.get(id=id)
    items = food_item.objects.filter(category_id=cat).values()
    print(items)
    return JsonResponse(list(items),safe=False)

def contact(request):
        if request.method == "POST":
                n = request.POST['name']
                e = request.POST['email']
                p = request.POST['phone']
                m = request.POST['message']

                data = feedback(name=n,email=e,phone=p,message=m)
                data.save()
                msz='Dear {}, Thanks for your feedback!!!'.format(n)
                return render(request,'contact.html',{'status':msz}) 

        return render(request,'contact.html',{'all':all,'ncat':all_cats,'nrest':new_rests}) 

def add_cart(request):
        cust = User.objects.get(username=request.user.username)
        viewCart = cart.objects.filter(user_id=cust,type=1).order_by('-id')
        total_cart = []
        if request.method == 'POST':
                item = request.POST['item_id']
                price = request.POST['amount']
                qty = request.POST['add']

                check = cart.objects.filter(type=1)
                product_object = food_item.objects.get(id=item)
                exist = False
                for x in check:
                        if x.product_id==product_object and x.user_id==cust:
                                exist=True          
                if exist is True:  
                        messages.warning(request,'Item Already in Your Cart')
                else:
                        saveCart = cart(user_id=cust, product_id=product_object, quantity=qty,type=1)
                        saveCart.save()
                        messages.success(request,'Item Added to Your Cart')                
        cid = ''
        for x in viewCart:
            cid+=str(x.id)+'/'
            user_cart = {}
            user_cart['id'] = x.id
            user_cart['product_name'] = x.product_id.item_name
            user_cart['restaurant'] = x.product_id.rest_id.name
            user_cart['r_city'] = x.product_id.rest_id.city
            user_cart['photo'] = x.product_id.image
            user_cart['product_cat'] = x.product_id.category_id.name
            user_cart['quantity'] = x.quantity
            user_cart['price'] = x.product_id.price
            user_cart['preparation_time'] = x.product_id.preparation_time
            user_cart['type'] = x.product_id.type
            user_cart['total'] = (x.quantity * x.product_id.price)
            total_cart.append(user_cart)
        if 'q' in request.GET:
            newQ = request.GET['q']
            cartId = request.GET['id']
            getCart = cart.objects.get(id=cartId)
            getCart.quantity = newQ            
            getCart.save() 
            details = {
                'quantity':getCart.quantity,
                'price':getCart.product_id.price,
                'cart_size':len(viewCart),
            }          
            return JsonResponse(details)
        return render(request,'cart.html',{'cart':total_cart,'all':all,'ncat':all_cats,'nrest':new_rests,'cart_size':len(total_cart)})
        
@login_required
def removeCart(request):
        if request.method=='GET':
                id = request.GET['removeid']
                deleteObj = cart.objects.get(id=id)
                deleteObj.delete()

                #User Cart 
                cust = User.objects.get(username=request.user.username)
                viewCart = cart.objects.filter(user_id=cust,type=1)
                cart_size = len(viewCart)
                data = {
                'msz':'Item Removed Successfully!!!',
                'cart_size':cart_size,
                }
        return JsonResponse(data)
        

def uslogin(request):
        status=0
        if request.method == "POST":
                if 'un'in request.POST:
                        usn = request.POST['un']
                        pas = request.POST['pwd']
                        user = authenticate(username= usn, password=pas)
                        if user:
                                if user.is_staff:
                                        login(request,user)
                                        response = HttpResponseRedirect('/mysite/delivery_partner')
                                        if "remember" in request.POST:
                                                response.set_cookie('username', usn)
                                                response.set_cookie('id',user.id)
                                                response.set_cookie('last_connection', datetime.datetime.now())
                                                return response
                                        else:
                                                return response
                                else:
                                        status = "You are is not activated!!"
                                if user.is_active:
                                        login(request,user)
                                        response = HttpResponseRedirect('/mysite/cart')
                                        if "remember" in request.POST:
                                                response.set_cookie('username', usn)
                                                response.set_cookie('id',user.id)
                                                response.set_cookie('last_connection', datetime.datetime.now())
                                                return response
                                        else:
                                                return response
                                else:
                                        status = "You are is not activated!!"
                        else:
                                status ="User Not Registered"
        return render(request,'cart.html',{'status':status,'all':all,'ncat':all_cats,'nrest':new_rests})
@login_required
def grandTotal(request):
        cust = User.objects.get(username=request.user.username)
        cc = customer.objects.get(user=cust)
        viewCart = cart.objects.filter(user_id=cust,type=1)
        grand_total = 0
        quantity = 0
        cid = ''
        for i in viewCart:
                cid += str(i.id)+'/@'
                grand_total = grand_total+(i.product_id.price*i.quantity)
                quantity = quantity+i.quantity
                
        data = {
                'cids':cid,
                'grand_total':grand_total,
                'quantity':quantity,
                'email':cust.username,
                'name':cust.first_name,
                'address':cc.city
        }

        return JsonResponse(data)
def checkout(request):
        cust = User.objects.get(username=request.user.username)
        custom = customer.objects.get(user=cust)
        if request.method == "POST":
                cids = request.POST['cids']
                grandtotal = request.POST['grandtotal']
                qty = request.POST['quantity']
                all = []
                for i in cids.split('/@')[:-1]:
                        items ={}
                        obj = cart.objects.get(id=i)
                        items.update({'name':obj.product_id.item_name,'price':obj.product_id.price,'quan':obj.quantity})
                        all.append(items)
                return render(request,'checkout.html',{'customer':custom,'grand':grandtotal,'csize':qty,'all':all,'cids':cids,'all':all,'ncat':all_cats,'nrest':new_rests})

@login_required
def get_order(request):
        us = User.objects.get(username=request.user.username)
        cust = customer.objects.get(user=us)
        if request.method=='GET':
                dnm = request.GET['dname']
                dem = request.GET['demail']
                dmn = request.GET['dnumber']
                addr = request.GET['daddress']
                amt = request.GET['grandtotal']
                items = request.GET['items']

                order_obj = order(customer=cust,amount=amt,contact_name=dnm,contact_number=dmn,contact_email=dem,delivery_address=addr,cart_id=items)
                order_obj.save()
                oid = order_obj.id

                return HttpResponse(oid)
def get_delivery_boy():
        dp = len(delivery_partner.objects.all())
        part = random.randint(1,dp+1)
        boy = delivery_partner.objects.get(id=part)
        if boy.is_available==True:
                boy.is_available=False
                boy.save()
                return boy
        else:
                get_delivery_boy()
@login_required
def success_payment(request):
    if 'ORDERID' in request.GET:
        order_id = request.GET['ORDERID']
        txn_id = request.GET['TXNID']
        pay_mode = request.GET['PAYMENTMODE']
        bank_name = request.GET['BANKNAME']

        sid = order_id.split('o')[0]
        order_obj = order.objects.get(id=int(sid))
        order_obj.txn_id = txn_id
        order_obj.payment_mode= pay_mode
        order_obj.bank_name = bank_name
        order_obj.save()

        boy = get_delivery_boy()
        order_obj.assigned_to = boy
        order_obj.save()
        all_items = order_obj.cart_id
        if all_items!='' or all_items !=None:
                ls = all_items.split('/@')
                for cid in ls[:-1]:
                        citem = cart.objects.get(id=cid)
                        citem.type=2
                        citem.save()
        try:
                em = order_obj.contact_email
                msz = "Dear {} Your Order received\n Here is your Txn ID: {}, Thanks for choosing Zykaa for shopping!!!".format(order_obj.contact_name,order_obj.txn_id)
                m=EmailMessage('Zykaa Order Received Conformation',msz,[em,])
                m.send()
        except:
                return render(request,'process.html',{'txn_id':txn_id,'order':order_obj,'all':all,'ncat':all_cats,'nrest':new_rests})

        return render(request,'process.html',{'txn_id':txn_id,'order':order_obj,'all':all,'ncat':all_cats,'nrest':new_rests})

def uslogout(request):
    logout(request)
    response = HttpResponseRedirect('/')
    response.delete_cookie('username')
    response.delete_cookie('id')
    return response

def order_history(request):
        us = User.objects.get(username=request.user.username)
        cust = customer.objects.get(user=us)
        orders = order.objects.filter(customer=cust).order_by('-id')
        all_data=[]
        for i in orders:
                ordrs = {
                        'id':i.id,
                        'txn_id':i.txn_id,
                        'payment_mode':i.payment_mode,
                        'received_on':i.received_on,
                        'delivered_at':i.delivery_address,
                        'amount':i.amount,
                        'status':i.status,

                }
                arr=[]
                for j in i.cart_id.split('/@')[:-1]:
                        items = {}
                        data = cart.objects.get(id=j)
                        items.update({'name':data.product_id.item_name,'price':data.product_id.price,'quantity':data.quantity})
                        arr.append(items)
                ordrs.update({'items':arr})
                all_data.append(ordrs)
        return render(request,'order_history.html',{'orders':all_data,'total':len(orders),'all':all,'ncat':all_cats,'nrest':new_rests})

def restaurant(request):
        all = Restaurant.objects.all()
        return render(request,'restaurant.html',{'all':all,'total':len(all)})

def viewmenu(request):
        if request.method =="GET":
                id = request.GET['id']
                rest = Restaurant.objects.get(id=id)
                food = food_item.objects.filter(rest_id=rest)
                cats = []
                for i in food:
                        if i.category_id in cats:
                                pass
                        else:
                                cats.append(i.category_id)

                return render(request,'menu-list.html',{'rest':rest,'all':food,'cats':cats,'all':all,'ncat':all_cats,'nrest':new_rests})

def allitems(request):  
        all = food_item.objects.all().order_by('-id')
        if request.method == "POST":
                print(request.POST)
                if request.POST["city"] != "":
                        ci = request.POST['city']                
                        it=food_item.objects.filter(rest_id__address__icontains=ci,rest_id__city__name__icontains=ci)
                        return render(request,'allfood.html',{'all':it,'total':len(it),'ci':ci,'all':all,'ncat':all_cats,'nrest':new_rests})
                if request.POST["dish"] != "":
                        ds = request.POST['dish']                
                        itm=food_item.objects.filter(item_name__contains=ds)
                        print('all hkh=',itm)
                        return render(request,'allfood.html',{'all':itm,'total':len(itm),'ci':ds,'all':all,'ncat':all_cats,'nrest':new_rests})
        return render(request,'allfood.html',{'all':all,'total':len(all),'all':all,'ncat':all_cats,'nrest':new_rests})

def search_cat(request):
        if "cid" in request.GET:
                ci = request.GET['cid']
                name = request.GET['name']
                all  = food_item.objects.filter(category_id__id__iexact=ci)
                return render(request,'category.html',{'all':all,'total':len(all),'ci':name,'all':all,'ncat':all_cats,'nrest':new_rests})

## get user
def getloginuser(id):
    ruser = User.objects.get(username=id)
    cust = customer.objects.get(user=ruser)
    details = {
        'id':ruser.id,
        'first_name':ruser.first_name,
        'letter':ruser.first_name[0],
        'username':ruser.username,
        'contact':cust.contact,
        'city':cust.city,
        'custid':cust.id,
        'registered_on':cust.registred_on,
    }
    return details

@login_required
def profile(request):
    details = getloginuser(request.user.username)
    return render(request,'profile.html',{'pp':details,'all':all,'ncat':all_cats,'nrest':new_rests})

@login_required
def change_profile(request):
    details = getloginuser(request.user.username)
    if request.method == 'POST':
        first_name = request.POST['name']
        email = request.POST['email']
        address = request.POST['address']
        contact = request.POST['contact']
        userid = request.POST['id']
        custid = request.POST['custid']

        userObj = User.objects.get(id=userid)
        if first_name != '':
            userObj.first_name = first_name
        if email != '': 
            userObj.email = email
           
        userObj.save()

        customerObj = customer.objects.get(id=custid)
        
        if address != '':
            customerObj.city = address
        if contact != '':
            customerObj.contact = contact
        customerObj.save()
        return render(request,'change_profile.html',{'pp':details,'status':'Changes saved successfully!!!','all':all,'ncat':all_cats,'nrest':new_rests})     
        
    return render(request,'change_profile.html',{'pp':details,'all':all,'ncat':all_cats,'nrest':new_rests})

def change_password(request):
    if request.method == 'POST':
        uid = request.POST['id']
        old = request.POST['oldpass']
        npass = request.POST['newpass']

        user = User.objects.get(id=uid)
        if check_password(old,user.password):
            user.set_password(npass)
            user.save()
            messages.success(request,'Password Changed!!!')
            
        else:
            messages.error(request,'Incorrect Current Password')
    return render(request,'change_password.html',{'pp':getloginuser(request.user.username),'all':all,'ncat':all_cats,'nrest':new_rests})

def about(request):
        return render(request,'about-us.html',{'all':all,'ncat':all_cats,'nrest':new_rests})

@login_required
def deliverypartner(request):
        usr = delivery_partner.objects.get(user__username=request.user.username)
        data = order.objects.filter(assigned_to=usr).order_by('-id')
        ordrs=[]
        for i in data:
                alldata = {
                        'id':i.id,
                        'txn_id':i.txn_id,
                        'payment_mode':i.payment_mode,
                        'received_on':i.received_on,
                        'delivered_at':i.delivery_address,
                        'delivered_to':i.customer.user.first_name,
                        'contact_number':i.contact_name,
                        'amount':i.amount,
                        'status':i.status,
                }
                ordrs.append(alldata)
        return render(request,'partnerorders.html',{'orders':ordrs,'total':len(ordrs),'all':all,'ncat':all_cats,'nrest':new_rests})
import datetime
def changestatus(request):
        if 'id' in request.GET:
                id = request.GET['id']
                user = request.GET['un']

                ord = order.objects.get(id=id)
                ord.status = 'Delivered'
                ord.delivered_on = datetime.datetime.now()
                ord.save()

                db = delivery_partner.objects.get(user__username=user)
                db.is_available = True
                db.save()
                st = ord.status
                try:
                        em = ord.contact_email
                        msz = "Hii {},\n Your Order with Txn ID:{} delivered successfully on {} by {}\n Thanks for choosing Zykaa for shopping!!!".format(order_obj.contact_name,order_obj.txn_id,order_obj.delivered_on,order_obj.assigned_to)
                        m=EmailMessage('Zykaa Order Received Conformation',msz,to=[em,])
                        m.send()
                except:
                        return HttpResponse(st)
                return HttpResponse(st)
        else:
                return HttpResponse('Something Went Wrong Try Again!!')

@login_required
def dprofile(request):
        data = delivery_partner.objects.get(user__username=request.user.username)
        return render(request,'dprofile.html',{'pp':data,'all':all,'ncat':all_cats,'nrest':new_rests})

def dsignup(request):
        if request.method =="POST":
                fn=request.POST['first_name']
                ln=request.POST['last_name']
                em=request.POST['email']
                ps=request.POST['password']
                gn=request.POST['gender']
                ag=request.POST['age']
                ct=request.POST['city']
                cnt=request.POST['contact']
                pf=request.FILES['profile']

                user = User.objects.create_user(em,em,ps)
                user.first_name=fn
                user.last_name=ln
                user.is_staff=True
                user.save()

                dp = delivery_partner(user=user,subtitle=gn,age=ag,city=ct,contact=cnt,profile_pic=pf)
                dp.save()
                return render(request,'dsignup.html',{'status':'Registred Successfully!!!','all':all,'ncat':all_cats,'nrest':new_rests})

        return render(request,'dsignup.html')